﻿
#include <iostream>
#include <cmath>

class Cat {
public:
    Cat() : name_("Barsik"), age_(4), weight_(5.5f)
    {}
    Cat(std::string name, int age, float weight)
        : name_(name), age_(age), weight_(weight)
    {}
    void show() {
        std::cout << "Cat name: " << name_ << ", age: " << age_ <<
                    ", weight: " << weight_ << " kg." << std::endl;
    }
private:
    std::string name_;
    int age_;
    float weight_;
};


class Vector {
public:
    Vector() : x_(0), y_(0), z_(0)
    {}
    Vector(double x, double y, double z) : x_(x), y_(y), z_(z)
    {}
    void show() {
        std::cout << "(" << x_ << " " << y_ << " " << z_ << ")" << std::endl;
    }
    double length() {
        return std::sqrt(x_ * x_ + y_ * y_ + z_ * z_);
    }
private:
    double x_;
    double y_;
    double z_;
};

int main()
{
    std::cout << "1. Class Cat." << std::endl;
    Cat cat, pussycat("Syma", 3, 4.3f);
    cat.show();
    pussycat.show();

    std::cout << std::endl;
    std::cout << "2. Class Vector." << std::endl;
    Vector v(3, 4, 5);
    std::cout << "Vector: ";
    v.show();
    std::cout << "Vector length: " << v.length();

    return 0;
}
